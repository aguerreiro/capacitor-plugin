package com.crea

import com.getcapacitor.*

@NativePlugin
class FormPlugin : Plugin() {
    @PluginMethod
    fun showForm(call: PluginCall) {
        val value = call.getString("value")
        val ret = JSObject()
        ret.put("value", value)
        call.success(ret)
    }
}