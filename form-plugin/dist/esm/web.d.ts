import { WebPlugin } from '@capacitor/core';
import { FormPluginPlugin } from './definitions';
export declare class FormPluginWeb extends WebPlugin implements FormPluginPlugin {
    constructor();
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
}
declare const FormPlugin: FormPluginWeb;
export { FormPlugin };
